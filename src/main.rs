use clap::Parser;
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use midir::MidiInput;
use std::{
    error::Error,
    io::{stdin, stdout, Write},
    path::{Path, PathBuf},
    sync::mpsc::Receiver,
};

use oxisynth::MidiEvent;

pub struct SynthBackend {
    _host: cpal::Host,
    device: cpal::Device,

    stream_config: cpal::StreamConfig,
    sample_format: cpal::SampleFormat,
}

impl SynthBackend {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        let host = cpal::default_host();

        let device = host
            .default_output_device()
            .ok_or("failed to find a default output device")?;

        let config = device.default_output_config()?;
        let sample_format = config.sample_format();

        let stream_config: cpal::StreamConfig = config.into();

        Ok(Self {
            _host: host,
            device,

            stream_config,
            sample_format,
        })
    }

    fn run<T: cpal::Sample>(&self, rx: Receiver<MidiEvent>, path: &Path) -> cpal::Stream {
        let mut synth = {
            let sample_rate = self.stream_config.sample_rate.0 as f32;

            let settings = oxisynth::SynthDescriptor {
                sample_rate,
                gain: 1.0,
                ..Default::default()
            };

            let mut synth = oxisynth::Synth::new(settings).unwrap();
            let mut file = std::fs::File::open(path).unwrap();
            let font = oxisynth::SoundFont::load(&mut file).unwrap();

            synth.add_font(font, true);
            synth.set_sample_rate(sample_rate);
            synth.set_gain(1.0);

            synth
        };

        let mut next_value = move || {
            let (l, r) = synth.read_next();

            if let Ok(e) = rx.try_recv() {
                synth.send_event(e).ok();
            }

            (l, r)
        };

        let err_fn = |err| eprintln!("an error occurred on stream: {}", err);

        let channels = self.stream_config.channels as usize;

        let stream = self
            .device
            .build_output_stream(
                &self.stream_config,
                move |output: &mut [T], _: &cpal::OutputCallbackInfo| {
                    for frame in output.chunks_mut(channels) {
                        let (l, r) = next_value();

                        let l: T = cpal::Sample::from::<f32>(&l);
                        let r: T = cpal::Sample::from::<f32>(&r);

                        let channels = [l, r];

                        for (id, sample) in frame.iter_mut().enumerate() {
                            *sample = channels[id % 2];
                        }
                    }
                },
                err_fn,
            )
            .unwrap();
        stream.play().unwrap();

        stream
    }

    pub fn new_output_connection<P: AsRef<Path>>(
        &mut self,
        path: &P,
    ) -> (cpal::Stream, SynthOutputConnection) {
        let (tx, rx) = std::sync::mpsc::channel::<MidiEvent>();
        let _stream = match self.sample_format {
            cpal::SampleFormat::F32 => self.run::<f32>(rx, path.as_ref()),
            cpal::SampleFormat::I16 => self.run::<i16>(rx, path.as_ref()),
            cpal::SampleFormat::U16 => self.run::<u16>(rx, path.as_ref()),
        };

        (_stream, SynthOutputConnection { tx })
    }
}

pub struct SynthOutputConnection {
    tx: std::sync::mpsc::Sender<MidiEvent>,
}

impl SynthOutputConnection {
    fn note_on(&mut self, channel: u8, key: u8, vel: u8) {
        self.tx.send(MidiEvent::NoteOn { channel, key, vel }).ok();
    }
    fn note_off(&mut self, channel: u8, key: u8) {
        self.tx.send(MidiEvent::NoteOff { channel, key }).ok();
    }
    fn cc(&mut self, channel: u8, ctrl: u8, value: u8) {
        self.tx
            .send(MidiEvent::ControlChange {
                channel,
                ctrl,
                value,
            })
            .ok();
    }
    fn pitch_bend(&mut self, channel: u8, value: u16) {
        self.tx.send(MidiEvent::PitchBend { channel, value }).ok();
    }
    fn program_change(&mut self, channel: u8, program_id: u8) {
        self.tx
            .send(MidiEvent::ProgramChange {
                channel,
                program_id,
            })
            .ok();
    }
}

#[derive(Parser, Debug)]
#[command(version, author, about, long_about = None)]
struct Args {
    #[arg(short, long, default_value = "0")]
    device: usize,
    #[arg(short, long, default_value = "/usr/share/soundfonts/FluidR3_GM.sf2")]
    soundfont: PathBuf,
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut synth = SynthBackend::new()?;

    let args = Args::parse();
    let mut input = String::new();
    let midi_in = MidiInput::new("midir reading input")?;

    // Get an input port (read from console if multiple are available)
    let in_ports = midi_in.ports();
    // Set in_port if passed as argument in matches
    let in_port = match in_ports.len() {
        0 => return Err("no input port found".into()),
        1 => {
            println!(
                "Choosing the only available input port: {}",
                midi_in.port_name(&in_ports[0]).unwrap()
            );
            &in_ports[0]
        }
        _ => match in_ports.get(args.device) {
            Some(port) => port,
            None => {
                println!("\nAvailable input ports:");
                for (i, p) in in_ports.iter().enumerate() {
                    println!("{}: {}", i, midi_in.port_name(p).unwrap());
                }
                print!("Please select input port: ");
                stdout().flush()?;
                let mut input = String::new();
                stdin().read_line(&mut input)?;
                in_ports
                    .get(input.trim().parse::<usize>()?)
                    .ok_or("invalid input port selected")?
            }
        },
    };
    let in_port_name = midi_in.port_name(&in_port)?;
    // Scan /usr/share/soundfonts for soundfonts and prompt for a selection if none is passed as argument

    let mut soundfont = args.soundfont;

    if !soundfont.is_file() {
        let mut soundfonts = std::fs::read_dir("/usr/share/soundfonts")?
            .filter_map(|entry| {
                let entry = entry.ok()?;
                let path = entry.path();
                if path.is_file() {
                    Some(path)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();

        soundfonts.sort();

        println!("\nAvailable soundfonts:");
        for (i, p) in soundfonts.iter().enumerate() {
            println!("{}: {}", i, p.display());
        }
        print!("Please select soundfont: ");
        stdout().flush()?;
        let mut input = String::new();
        stdin().read_line(&mut input)?;
        soundfont = soundfonts
            .get(input.trim().parse::<usize>()?)
            .ok_or("invalid soundfont selected")?
            .to_owned();
    };

    let (_stream, mut synth_conn) = synth.new_output_connection(&soundfont);

    // _conn_in needs to be a named parameter, because it needs to be kept alive until the end of the scope
    let _conn_in = midi_in.connect(
        &in_port,
        "midir-read-input",
        move |stamp, message, _| {
            match message.len() {
                2 => {
                    println!("{}: {:?} (len = {})", stamp, message, message.len());
                    let channel = message[0];
                    match channel {
                        192 => synth_conn.program_change(0, message[1]),
                        194 => synth_conn.program_change(1, message[1]),
                        206 => synth_conn.program_change(2, message[1]),
                        _ => println!("Unknown channel {}", channel),
                    }
                }
                3 => {
                    let channel = message[0];
                    let note = message[1];
                    let vel = message[2];
                    match channel {
                        128 => {
                            println!("NoteOff {}", note);
                            if note >= 21 && note <= 108 {
                                synth_conn.note_off(0, note);
                            } else {
                                synth_conn.note_off(9, note);
                            }
                        }
                        144 => {
                            println!("NoteOn {},{}", note, vel);
                            if note >= 21 && note <= 108 {
                                synth_conn.note_on(0, note, vel);
                            } else {
                                synth_conn.note_on(9, note, vel);
                            }
                        }
                        146 => synth_conn.note_on(1, note, vel),
                        130 => synth_conn.note_off(1, note),
                        158 => synth_conn.note_on(2, note, vel),
                        142 => synth_conn.note_off(2, note),
                        178 | 176 | 190 => {
                            // R dials on A88-MK2
                            match note {
                                1 => synth_conn.cc(0, 1, vel), // vibrato
                                3 => println!("R1 {}", vel),
                                9 => println!("R2 {}", vel),
                                89 => println!("R3 {}", vel),
                                97 => println!("R4 {}", vel),
                                16 => println!("R5 {}", vel),
                                17 => println!("R6 {}", vel),
                                29 => println!("R7 {}", vel),
                                5 => println!("R8 {}", vel),
                                _ => println!(
                                    "No knob handler set up {}: {:?} (len = {})",
                                    stamp,
                                    message,
                                    message.len()
                                ),
                            }
                        }
                        177 => {
                            println!("vibra {}: {:?} (len = {})", stamp, message, message.len());
                            synth_conn.cc(0, note, vel);
                        }
                        224 | 226 | 238 => {
                            println!("bending {}: {:?} (len = {})", stamp, message, message.len());
                            synth_conn.pitch_bend(2, vel as u16);
                        }
                        _ => println!("{}: {:?} (len = {})", stamp, message, message.len()),
                    };
                }
                _ => println!("{:?}", message),
            }
        },
        (),
    )?;

    println!(
        "Connection open, reading input from '{}' (press enter to exit) ...",
        in_port_name
    );
    input.clear();
    stdin().read_line(&mut input)?; // wait for next enter key press

    println!("Closing connection");
    Ok(())
}
